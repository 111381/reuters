package ee.energia.watson.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.Connection;

@Component
public class DbHelper {

  @Autowired
  private ApplicationContext applicationContext;

  public Connection getSqlConnection() {
    DataSource ds = (DataSource) this.applicationContext.getBean("dataSource");
    return DataSourceUtils.getConnection(ds);
  }
}
