package ee.energia.watson.utils;

import com.sun.istack.internal.NotNull;
import ee.energia.watson.constants.FuturesAndOptionsType;
import ee.energia.watson.domain.Price;
import ee.energia.watson.domain.Security;
import ee.energia.watson.domain.SecurityType;
import ee.energia.watson.domain.Symbology;
import ee.energia.watson.repository.PricesRepository;
import ee.energia.watson.repository.SecurityRepository;
import ee.energia.watson.repository.SecurityTypeRepository;
import ee.energia.watson.repository.SymbologyRepository;
import org.apache.http.Header;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@Component
public class HttpClientHelper {

  @Autowired
  SecurityRepository securityRepository;
  @Autowired
  private PricesRepository pricesRepository;
  @Autowired
  private SecurityTypeRepository securityTypeRepository;
  @Autowired
  private SymbologyRepository symbologyRepository;

  private static final Logger LOG = LoggerFactory.getLogger(HttpClientHelper.class);
  private static final String HEADER_PREFER = "Prefer ";
  private static final String HEADER_RESPOND_ASYNC = "respond-async";

  public HttpPost newPostWithFilledHeadersAndBody(String fullPath, @NotNull List<Header> headers, @NotNull JSONObject requestBody) throws UnsupportedEncodingException {
    HttpPost httpPost = new HttpPost(fullPath);
    //default headers
    httpPost.addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
    httpPost.addHeader(HEADER_PREFER, HEADER_RESPOND_ASYNC);
    //additional headers
    for(Header header : headers) {
      httpPost.addHeader(header);
    }
    //body
    httpPost.setEntity(new StringEntity(requestBody.toString()));
    return httpPost;
  }

  public void checkResponseStatus(HttpResponse httpResponse) throws ClientProtocolException {
    int statusCode = httpResponse.getStatusLine().getStatusCode();
    switch(statusCode) {
      case 200:
        break;
      case 400:
        throw new ClientProtocolException("HTTP status 400. Request content is malformed, cannot be parsed.");
      case 401:
        throw new ClientProtocolException("HTTP status: 401. Authentication token is invalid or expired.");
    }
  }

  public JSONObject fetchResponseToJsonObject(HttpResponse httpResponse) throws IOException {
    BufferedReader rd = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
    StringBuffer result = new StringBuffer();
    String line = "";
    while((line = rd.readLine()) != null) {
      result.append(line);
    }
    LOG.info("response: " + result.toString());
    JSONObject jsonResponse = new JSONObject(result.toString());
    return jsonResponse;
  }

  public Boolean checkIfFutureSwapOption(Security parentSecurity) {
    List<String> list = new ArrayList<>();
    list.add(FuturesAndOptionsType.FUTURE.getValue());
    list.add(FuturesAndOptionsType.OPTION.getValue());
    list.add(FuturesAndOptionsType.SWAP.getValue());
    for(SecurityType st : this.securityTypeRepository.findByCodeIn(list)) {
      if(st.getId() == parentSecurity.getSecurityTypeId()) {
        return Boolean.TRUE;
      }
    }
    return Boolean.FALSE;
  }

  public Security getSecurity(BigInteger id) {
    return securityRepository.findOne(id);
  }

  public void savePrices(List<Price> priceList) {
    pricesRepository.save(priceList);
  }

  public List<Symbology> getSymbologyList() {
    return (List) symbologyRepository.findAll();
  }
}
