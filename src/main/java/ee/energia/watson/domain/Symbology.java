package ee.energia.watson.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigInteger;

@Data
@NoArgsConstructor
@Entity
public class Symbology {
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE)
  private BigInteger id;
  @NonNull
  private BigInteger vendorId;
  @NonNull
  private BigInteger securityId;
  @NonNull
  private String code;
}
