package ee.energia.watson.domain;

import lombok.Data;
import lombok.NonNull;
import org.json.JSONObject;

import java.math.BigInteger;
import java.sql.Timestamp;

@Data
public class RawData {

  private BigInteger id;
  private Timestamp insertedTs;
  @NonNull
  private JSONObject request;
  @NonNull
  private JSONObject response;
  @NonNull
  private String dataSource;
}
