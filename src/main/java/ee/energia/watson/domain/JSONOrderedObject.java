package ee.energia.watson.domain;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Extension to {@link JSONObject} that remembers the order of inserts.
 */
@SuppressWarnings("rawtypes")
public class JSONOrderedObject extends JSONObject {

  private Set<String> keys = new LinkedHashSet<>();

  @Override
  public JSONObject put(String key, Object value) throws JSONException {
    this.keys.add(key);
    return super.put(key, value);
  }

  @Override
  public Iterator keys() {
    return this.keys.iterator();
  }

}
