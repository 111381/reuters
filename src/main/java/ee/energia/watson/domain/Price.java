package ee.energia.watson.domain;

import lombok.Data;
import org.springframework.data.annotation.Transient;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Date;
import java.sql.Timestamp;

@Data
@Entity
public class Price {
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE)
  private Integer id;
  private BigInteger securityId;
  private Timestamp date;
  private Integer length;
  private BigDecimal open;
  private BigDecimal high;
  private BigDecimal low;
  private BigDecimal close;
  private BigDecimal settle;
  private BigInteger volume;
  private Integer count;
  private BigInteger sourceId;
  private BigInteger marketId;
  private Timestamp updateDate;
  @Transient
  private Date expiry;
}
