package ee.energia.watson.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Date;

@Data
@NoArgsConstructor
@Entity
public class Security {
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE)
  private BigInteger id;
  private String name;
  @Column(unique = true)
  private BigInteger securityTypeId;
  @Column(unique = true)
  private BigInteger securityRightId;
  @Column(unique = true)
  private BigInteger assetTypeId;
  @Column(unique = true)
  private String symbol;
  @Column(unique = true)
  private BigInteger currencyId;
  @Column(unique = true)
  private BigInteger deliveryAreaId;
  private Integer lotSize;
  private BigDecimal tickSize;
  private BigDecimal settleSize;
  private Byte[] blurb;
  private BigInteger unitId;
  @Column(unique = true)
  private BigDecimal strike;
  @Column(unique = true)
  private Date expiry;
  private Integer chainSize;
  private BigInteger granularityId;
  private BigInteger marketId;
  private BigInteger parentId;
  private Integer isActive;
  private Integer isValidated;

  public Security(Security s) {
    this.name = s.name;
    this.securityTypeId = s.securityTypeId;
    this.securityRightId = s.securityRightId;
    this.assetTypeId = s.assetTypeId;
    this.symbol = s.symbol;
    this.currencyId = s.currencyId;
    this.deliveryAreaId = s.deliveryAreaId;
    this.lotSize = s.lotSize;
    this.tickSize = s.tickSize;
    this.settleSize = s.settleSize;
    this.blurb = s.blurb;
    this.unitId = s.unitId;
    this.strike = s.strike;
    this.expiry = s.expiry;
    this.chainSize = s.chainSize;
    this.granularityId = s.granularityId;
    this.marketId = s.marketId;
    this.parentId = s.parentId;
    this.isActive = s.isActive;
    this.isValidated = s.isValidated;
  }
}
