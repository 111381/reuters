package ee.energia.watson.repository;

import ee.energia.watson.domain.Security;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Date;
import java.util.List;

@Repository
public interface SecurityRepository extends CrudRepository<Security, BigInteger> {

  List<Security> findSecuritiesByParentIdEqualsAndIsActiveEquals(BigInteger parentId, Integer isActive);

  List<Security> findAllBySecurityTypeIdAndSecurityRightIdAndAssetTypeIdAndSymbolAndCurrencyIdAndDeliveryAreaIdAndStrikeAndExpiry(
      BigInteger securityTypeId, BigInteger securityRightId, BigInteger assetTypeId, String symbol, BigInteger currencyId, BigInteger deliveryAreaId, BigDecimal strike, Date expiry);
}
