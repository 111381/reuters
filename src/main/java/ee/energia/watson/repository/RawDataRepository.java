package ee.energia.watson.repository;

import ee.energia.watson.domain.RawData;
import ee.energia.watson.utils.DbHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@Repository
public class RawDataRepository {

  @Autowired
  DbHelper dbHelper;

  public void save(RawData rawData) throws SQLException {
    String sql = "INSERT INTO raw_data (request, response, data_source) VALUES (?::JSON, ?::JSON, ?)";
    Connection connection = dbHelper.getSqlConnection();
    PreparedStatement preparedStatement = null;
    try {
      preparedStatement = connection.prepareStatement(sql);
      int parameterIndex = 1;
      preparedStatement.setObject(parameterIndex++, rawData.getRequest().toString());
      preparedStatement.setObject(parameterIndex++, rawData.getResponse().toString());
      preparedStatement.setString(parameterIndex++, rawData.getDataSource());
      preparedStatement.executeUpdate();
    } finally {
      if(preparedStatement != null && !preparedStatement.isClosed()) preparedStatement.close();
      connection.close();
    }
  }
}
