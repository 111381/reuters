package ee.energia.watson.repository;

import ee.energia.watson.domain.Symbology;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;

@Repository
public interface SymbologyRepository extends CrudRepository<Symbology, BigInteger> {
}
