package ee.energia.watson.repository;

import ee.energia.watson.domain.Price;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PricesRepository extends CrudRepository<Price, Integer> {
}
