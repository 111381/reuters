package ee.energia.watson.repository;

import ee.energia.watson.domain.SecurityType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;

@Repository
public interface SecurityTypeRepository extends CrudRepository<SecurityType, BigInteger> {

  List<SecurityType> findByCodeIn(List<String> codes);
}
