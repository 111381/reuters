package ee.energia.watson.service;

import ee.energia.watson.constants.ApplicationConstants;
import ee.energia.watson.constants.RequestMethods;
import ee.energia.watson.constants.ValidityStatus;
import ee.energia.watson.domain.JSONOrderedObject;
import ee.energia.watson.domain.Price;
import ee.energia.watson.domain.RawData;
import ee.energia.watson.domain.Security;
import ee.energia.watson.repository.RawDataRepository;
import ee.energia.watson.repository.SecurityRepository;
import ee.energia.watson.utils.HttpClientHelper;
import org.apache.http.Header;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicHeader;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.NoResultException;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class HttpClientService {

  private static final Logger LOG = LoggerFactory.getLogger(HttpClientService.class);
  @Autowired
  RawDataRepository rawDataRepository;
  @Autowired
  SecurityRepository securityRepository;
  @Autowired
  private HttpClientHelper httpClientHelper;
  @Autowired
  private CloseableHttpClient httpclient;

  private List<Header> headers = new ArrayList<>();

  public void requestToken(String username, String password) {
    JSONObject requestBody = new JSONObject()
        .put("Credentials", new JSONObject()
            .put("Username", username)
            .put("Password", password));
    try {
      HttpPost httpPost = httpClientHelper.newPostWithFilledHeadersAndBody(ApplicationConstants.SERVICE_PATH + RequestMethods.REQUEST_TOKEN.getValue(), this.headers, requestBody);
      HttpResponse httpResponse = httpclient.execute(httpPost);
      httpClientHelper.checkResponseStatus(httpResponse);
      JSONObject jsonResponse = httpClientHelper.fetchResponseToJsonObject(httpResponse);
      this.headers.add(new BasicHeader(HttpHeaders.AUTHORIZATION, "Token " + jsonResponse.get("value").toString()));
    } catch(IOException e) {
      LOG.error(e.getLocalizedMessage());
    }
  }

  public List<String> futuresAndOptionsSearch(String identifier, String identifierType, String futuresAndOptionsType) {
    List<String> validFuturesAndOptionsArray = new ArrayList<>();
    JSONObject requestBody = new JSONObject()
        .put("SearchRequest", new JSONObject()
            .put("Identifier", identifier + "*")
            .put("IdentifierType", identifierType)
            .put("FuturesAndOptionsType", futuresAndOptionsType)
            .put("ExpirationDate", new JSONOrderedObject()
                .put("@odata.type", "#ThomsonReuters.Dss.Api.Search.DateValueComparison")
                .put("ComparisonOperator", "GreaterThanEquals")
                .put("Value", Instant.now().toString())
            )
        );
    try {
      this.fetchLink(ApplicationConstants.SERVICE_PATH + RequestMethods.FUTURES_AND_OPTIONS_SEARCH.getValue(), requestBody, validFuturesAndOptionsArray);
      LOG.info("Product identifiers list size: " + Integer.toString(validFuturesAndOptionsArray.size()) + " for type: " + identifierType);
      return validFuturesAndOptionsArray;
    } catch(IOException e) {
      LOG.error(e.getLocalizedMessage());
    } catch(JSONException e) {
      LOG.error(e.getLocalizedMessage());
    } catch(SQLException e) {
      LOG.error(e.getLocalizedMessage());
    }
    return validFuturesAndOptionsArray;
  }

  private void fetchLink(String link, JSONObject requestBody, List<String> validFuturesAndOptionsArray) throws IOException, SQLException {
    String nextLinkKey = "@odata.nextlink";
    String value = "value";
    LOG.info("link: " + link + ", request: " + requestBody.toString());
    HttpPost httpPost = httpClientHelper.newPostWithFilledHeadersAndBody(link, this.headers, requestBody);
    HttpResponse httpResponse = httpclient.execute(httpPost);
    httpClientHelper.checkResponseStatus(httpResponse);
    JSONObject jsonResponse = httpClientHelper.fetchResponseToJsonObject(httpResponse);
    rawDataRepository.save(new RawData(requestBody, jsonResponse, link));
    validFuturesAndOptionsArray.addAll(this.parseValidFuturesAndOptionsArray(jsonResponse.getJSONArray(value)));
    if(jsonResponse.has(nextLinkKey)) {
      LOG.info("NextLink: " + jsonResponse.getString(nextLinkKey));
      this.fetchLink(jsonResponse.getString(nextLinkKey), requestBody, validFuturesAndOptionsArray);
    }
  }

  private List<String> parseValidFuturesAndOptionsArray(JSONArray jsonArray) throws JSONException {
    List<String> identifiers = new ArrayList();
    for(Object o : jsonArray) {
      JSONObject jsonObject = (JSONObject) o;
      LOG.info(jsonObject.toString());
      if(jsonObject.get("Status").toString().equals(ValidityStatus.VALID.getValue())) {
        identifiers.add(jsonObject.get("Identifier").toString());
      }
    }
    return identifiers;
  }

  public JSONArray getClosingPrices(String identifierType, List<String> identifiers) {
    if(identifiers.isEmpty()) {
      return new JSONArray();
    }
    JSONArray jsonArray = new JSONArray();
    for(String id : identifiers) {
      jsonArray.put(new JSONObject()
          .put("Identifier", id)
          .put("IdentifierType", identifierType));
    }
    JSONObject requestBody = new JSONObject()
        .put("ExtractionRequest", new JSONOrderedObject()
            .put("@odata.type", "#ThomsonReuters.Dss.Api.Extractions.ExtractionRequests.EndOfDayPricingExtractionRequest")
            .put("ContentFieldNames", new JSONArray()
                .put("Trade Date")
                .put("Open Price")
                .put("High Price")
                .put("Low Price")
                .put("Close Price")
                .put("Volume")
                .put("Settlement Price")
                .put("Open Interest")
                .put("Volatility")
                .put("Lot Size")
                .put("Lot Units")
                .put("Expiration Date")
                .put("Exercise Style")
                .put("Exercise Type")
                .put("Market MIC")
                .put("ISIN")
                .put("Contract Type")
                .put("Asset Type")
                .put("Trading Symbol")
                .put("Currency Code")
                .put("Ticker")
                .put("RIC")
                .put("RIC Root")
                .put("Strike Price")
                .put("Start Date")
                .put("Units")
            ).put("IdentifierList", new JSONOrderedObject()
                .put("@odata.type", "#ThomsonReuters.Dss.Api.Extractions.ExtractionRequests.InstrumentIdentifierList")
                .put("InstrumentIdentifiers", jsonArray
                )
            ).put("Condition", JSONObject.NULL)
        );
    try {
      HttpPost httpPost = httpClientHelper.newPostWithFilledHeadersAndBody(ApplicationConstants.SERVICE_PATH + RequestMethods.EXTRACT_WITH_NOTES.getValue(), this.headers, requestBody);
      LOG.info(requestBody.toString());
      HttpResponse httpResponse = httpclient.execute(httpPost);
      httpClientHelper.checkResponseStatus(httpResponse);
      JSONObject jsonResponse = httpClientHelper.fetchResponseToJsonObject(httpResponse);
      rawDataRepository.save(new RawData(requestBody, jsonResponse, ApplicationConstants.SERVICE_PATH + RequestMethods.EXTRACT_WITH_NOTES.getValue()));
      return jsonResponse.getJSONArray("Contents");
    } catch(IOException e) {
      LOG.error(e.getLocalizedMessage());
    } catch(JSONException e) {
      LOG.error(e.getLocalizedMessage());
    } catch(SQLException e) {
      LOG.error(e.getLocalizedMessage());
    } catch(NoResultException e) {
      LOG.error(e.getLocalizedMessage());
    }
    return new JSONArray();
  }

  public List<Price> extractPriceResponse(JSONArray jsonArray) {
    List<Price> prices = new ArrayList<>();
    for(Object object : jsonArray) {
      JSONObject jsonObject = (JSONObject) object;
      LOG.info(jsonObject.toString());
      Price price = new Price();
      String expirationbDate = "Expiration Date";
      if(jsonObject.has(expirationbDate) && !jsonObject.isNull(expirationbDate)) {
        price.setExpiry(Date.valueOf(jsonObject.getString(expirationbDate)));
      }
      String tradeDate = "Trade Date";
      if(jsonObject.has(tradeDate) && !jsonObject.isNull(tradeDate)) {
        price.setDate(Timestamp.valueOf(jsonObject.getString(tradeDate) + " 00:00:00"));
      }
      int defaultLength = 86400;
      price.setLength(defaultLength);
      String openPrice = "Open Price";
      if(jsonObject.has(openPrice) && !jsonObject.isNull(openPrice)) {
        price.setOpen(jsonObject.getBigDecimal(openPrice));
      }
      String highPrice = "High Price";
      if(jsonObject.has(highPrice) && !jsonObject.isNull(highPrice)) {
        price.setHigh(jsonObject.getBigDecimal(highPrice));
      }
      String lowPrice = "Low Price";
      if(jsonObject.has(lowPrice) && !jsonObject.isNull(lowPrice)) {
        price.setLow(jsonObject.getBigDecimal(lowPrice));
      }
      String closePrice = "Close Price";
      if(jsonObject.has(closePrice) && !jsonObject.isNull(closePrice)) {
        price.setClose(jsonObject.getBigDecimal(closePrice));
      }
      String settlementPrice = "Settlement Price";
      if(jsonObject.has(settlementPrice) && !jsonObject.isNull(settlementPrice)) {
        price.setSettle(jsonObject.getBigDecimal(settlementPrice));
      }
      String volume = "Volume";
      if(jsonObject.has(volume) && !jsonObject.isNull(volume)) {
        price.setVolume(jsonObject.getBigInteger(volume));
      }
      String units = "Units";
      if(jsonObject.has(units) && !jsonObject.isNull(units)) {
        price.setCount(jsonObject.getInt(units));
      }
      price.setUpdateDate(Timestamp.from(Instant.now()));
      prices.add(price);
    }
    return prices;
  }

  public void updateSecurityTableAndPrices(List<Price> prices, Security parentSecurity) {
    if(httpClientHelper.checkIfFutureSwapOption(parentSecurity)) {
      List<Security> activeSecurities = securityRepository.findSecuritiesByParentIdEqualsAndIsActiveEquals(parentSecurity.getId(), 1);
      for(Security s : activeSecurities) {
        if(s.getExpiry() == null || s.getExpiry().before(Date.valueOf(LocalDate.now()))) {
          s.setIsActive(0);
          LOG.info("expired: " + securityRepository.save(s).toString());
        }
      }
      for(Price price : prices) {
        Security copyOfSecurity = new Security(parentSecurity);
        copyOfSecurity.setIsActive(1);
        copyOfSecurity.setExpiry(price.getExpiry());
        copyOfSecurity.setParentId(parentSecurity.getId());
        // check if new security is unique
        // TODO: for higher performance could be done with DB unique contraint and UPSERT functionality
        List<Security> foundCopies = securityRepository.findAllBySecurityTypeIdAndSecurityRightIdAndAssetTypeIdAndSymbolAndCurrencyIdAndDeliveryAreaIdAndStrikeAndExpiry(
            copyOfSecurity.getSecurityTypeId(),
            copyOfSecurity.getSecurityRightId(),
            copyOfSecurity.getAssetTypeId(),
            copyOfSecurity.getSymbol(),
            copyOfSecurity.getCurrencyId(),
            copyOfSecurity.getDeliveryAreaId(),
            copyOfSecurity.getStrike(),
            copyOfSecurity.getExpiry()
        );
        if(foundCopies.isEmpty()) {
          price.setSecurityId(securityRepository.save(copyOfSecurity).getId());
        } else if(foundCopies.size() == 1) {
          price.setSecurityId(foundCopies.get(0).getId());
          LOG.info("Price with existing security_id: " + price.toString());
        } else {
          LOG.error("Duplicate UNIQUE rows found");
        }
      }
      // Product without derivatives
    } else {
      for(Price price : prices) {
        price.setSecurityId(parentSecurity.getId());
      }
    }
  }
}
