package ee.energia.watson.constants;

public enum RequestMethods {

  REQUEST_TOKEN("/Authentication/RequestToken"),
  FUTURES_AND_OPTIONS_SEARCH("/Search/FuturesAndOptionsSearch"),
  EXTRACT_WITH_NOTES("/Extractions/ExtractWithNotes");

  private final String value;

  RequestMethods(String value) {
    this.value = value;
  }

  public String getValue() {
    return this.value;
  }
}
