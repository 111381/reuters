package ee.energia.watson.constants;

public enum ValidityStatus {

  VALID("Valid");

  private final String value;

  ValidityStatus(String value) {
    this.value = value;
  }

  public String getValue() {
    return this.value;
  }
}
