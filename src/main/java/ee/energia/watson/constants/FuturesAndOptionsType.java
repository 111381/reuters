package ee.energia.watson.constants;

public enum FuturesAndOptionsType {

  FUTURES("Futures"),
  FUTURES_ON_OPTION("FuturesOnOptions"),
  OPTIONS("Options"),
  FUTURE("Future"),
  OPTION("Option"),
  SWAP("Swap");

  private final String value;

  FuturesAndOptionsType(String value) {
    this.value = value;
  }

  public String getValue() {
    return this.value;
  }
}
