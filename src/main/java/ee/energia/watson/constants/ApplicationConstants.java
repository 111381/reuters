package ee.energia.watson.constants;

public final class ApplicationConstants {

  public static final String SERVICE_PATH = "https://hosted.datascopeapi.reuters.com/RestApi/v1";
  //TODO: handle credentials safely
  public static final String USER = "";
  public static final String PASS = "";
}
