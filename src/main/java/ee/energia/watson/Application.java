package ee.energia.watson;

import ee.energia.watson.constants.ApplicationConstants;
import ee.energia.watson.constants.FuturesAndOptionsType;
import ee.energia.watson.domain.Price;
import ee.energia.watson.domain.Security;
import ee.energia.watson.domain.Symbology;
import ee.energia.watson.service.HttpClientService;
import ee.energia.watson.utils.HttpClientHelper;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class Application {

  @Autowired
  private HttpClientService httpClientService;
  @Autowired
  private HttpClientHelper httpClientHelper;

  private CloseableHttpClient closeableHttpClient;

  public static void main(String args[]) {
    SpringApplication.run(Application.class);
  }

  @Bean
  public CloseableHttpClient closeableHttpClient() {
    this.closeableHttpClient = HttpClientBuilder.create().build();
    return this.closeableHttpClient;
  }

  @Bean
  public CommandLineRunner run() {
    return args -> {
      httpClientService.requestToken(ApplicationConstants.USER, ApplicationConstants.PASS);
      String identifierType = "Ric";
      List<String> identifiers = new ArrayList<>();
      List<Symbology> symbologyList;
      symbologyList = httpClientHelper.getSymbologyList(); //from DB
      //this.manualInput(symbologyList); // hardcoded
      for (Symbology symbology : symbologyList) {
        Security parentSecurity = httpClientHelper.getSecurity(symbology.getSecurityId());
        if (httpClientHelper.checkIfFutureSwapOption(parentSecurity)) {
          identifiers = httpClientService.futuresAndOptionsSearch(symbology.getCode(), identifierType, FuturesAndOptionsType.FUTURES.getValue());
        } else {
          identifiers.add(symbology.getCode());
        }
        JSONArray jsonArray = httpClientService.getClosingPrices(identifierType, identifiers);
        List<Price> prices = httpClientService.extractPriceResponse(jsonArray);
        httpClientService.updateSecurityTableAndPrices(prices, parentSecurity);
        httpClientHelper.savePrices(prices);
      }
      this.closeableHttpClient.close();
    };
  }

  private void manualInput(List<Symbology> symbologyList) {
    Symbology s = new Symbology();
    s.setCode("LGO");
    s.setSecurityId(new BigInteger("115"));
    symbologyList.add(s);
  }
}
